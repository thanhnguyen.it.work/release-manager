package com.example.manager.mapper;

import com.example.manager.entity.ServiceEntity;
import com.example.manager.model.ServiceModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(componentModel = "spring", uses = {})
public interface ServiceMapper {

    @Mapping(ignore = true, target = "id")
    @Mapping(ignore = true, target = "createdDate")
    @Mapping(ignore = true, target = "updatedDate")
    @Mapping(ignore = true, target = "systemVersionEntities")
    ServiceEntity toEntity(ServiceModel serviceModel);

    ServiceModel toModel(ServiceEntity service);

    List<ServiceModel> toModel(List<ServiceEntity> service);
}