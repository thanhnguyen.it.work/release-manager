package com.example.manager.model;

import lombok.Data;

@Data
public class ServiceModel {

    private String serviceName;

    private Integer serviceVersionNumber;
}
