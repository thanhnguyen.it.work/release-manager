package com.example.manager;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class ManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ManagerApplication.class, args);
        log.info("""

                ----------------------------------------------------------
                        Application 'release manager' is running! Access URLs:
                        Local: http://localhost:8080
                ----------------------------------------------------------
                        """
        );
    }

}
