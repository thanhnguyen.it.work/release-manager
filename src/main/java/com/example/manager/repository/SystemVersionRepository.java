package com.example.manager.repository;

import com.example.manager.entity.SystemVersionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SystemVersionRepository extends JpaRepository<SystemVersionEntity, Long> {

    Optional<SystemVersionEntity> findByNumber(int number);

    @Query(value = """
            select (coalesce(max(number), 0)) from system_version
            """, nativeQuery = true)
    int getCurrentSystemVersionNumber();
}
