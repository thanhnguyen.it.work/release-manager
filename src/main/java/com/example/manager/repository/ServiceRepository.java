package com.example.manager.repository;

import com.example.manager.entity.ServiceEntity;
import com.example.manager.entity.SystemVersionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ServiceRepository extends JpaRepository<ServiceEntity, Long> {
    List<ServiceEntity> findBySystemVersionEntities(SystemVersionEntity systemVersion);

    @Query(value = """    				
            select s.*
            from service s, service_system_version ssv, system_version sv
            where s.id = ssv.service_id and sv.id = ssv.system_version_id
            and sv.number= ?1
            """, nativeQuery = true)
    List<ServiceEntity> findCurrentlyDeployedServices(int systemVersionNumber);
}
