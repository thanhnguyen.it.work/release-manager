package com.example.manager.controller;

import com.example.manager.model.ServiceModel;
import com.example.manager.service.ManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class ManagerController {

    @Autowired
    private ManagerService managerService;

    @GetMapping("/services")
    public List<ServiceModel> getServiceBySystemVersion(@RequestParam int systemVersion) {
        return managerService.getServiceBySystemVersion(systemVersion);
    }

    @PostMapping("/deploy")
    public int deploy(@RequestBody ServiceModel serviceModel) {
        return managerService.deploy(serviceModel);
    }
}
