package com.example.manager.service;

import com.example.manager.entity.ServiceEntity;
import com.example.manager.entity.SystemVersionEntity;
import com.example.manager.mapper.ServiceMapper;
import com.example.manager.model.ServiceModel;
import com.example.manager.repository.ServiceRepository;
import com.example.manager.repository.SystemVersionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class ManagerService {

    @Autowired
    private ServiceRepository serviceRepository;

    @Autowired
    private SystemVersionRepository systemVersionRepository;

    @Autowired
    private ServiceMapper serviceMapper;

    public List<ServiceModel> getServiceBySystemVersion(int systemVersion) {
        Optional<SystemVersionEntity> systemVersionEntity = systemVersionRepository.findByNumber(systemVersion);
        if (!systemVersionEntity.isPresent()) {
            return Collections.emptyList();
        }
        return serviceMapper.toModel(systemVersionEntity.get().getServiceEntities());
    }

    @Transactional
    public int deploy(ServiceModel serviceModel) {
        // check if service is running
        List<ServiceEntity> currentlyDeployedServices = getCurrentlyDeployedServices();
        boolean exist = serviceMapper.toModel(currentlyDeployedServices).contains(serviceModel);

        if (exist) {
            return systemVersionRepository.getCurrentSystemVersionNumber();
        }

        // create a new service
        ServiceEntity serviceEntity = serviceRepository.save(serviceMapper.toEntity(serviceModel));

        SystemVersionEntity systemVersion = new SystemVersionEntity();

        // increase the system version number
        systemVersion.setNumber(systemVersionRepository.getCurrentSystemVersionNumber() + 1);

        // only link the latest systemversion for running services. remove the service with the changed version number
        currentlyDeployedServices.removeIf(service -> service.getServiceName().equals(serviceEntity.getServiceName()));
        currentlyDeployedServices.add(serviceEntity);
        systemVersion.setServiceEntities(currentlyDeployedServices);

        systemVersion = systemVersionRepository.save(systemVersion);
        return systemVersion.getNumber();
    }

    public List<ServiceEntity> getCurrentlyDeployedServices() {
        return serviceRepository.findCurrentlyDeployedServices(systemVersionRepository.getCurrentSystemVersionNumber());
    }
}
