package com.example.manager.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "service")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ServiceEntity extends BaseEntity {

    private String serviceName;

    private Integer serviceVersionNumber;

    @ManyToMany(mappedBy = "serviceEntities")
    private List<SystemVersionEntity> systemVersionEntities;
}
