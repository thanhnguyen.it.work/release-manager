package com.example.manager.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "system_version")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SystemVersionEntity extends BaseEntity {

    private int number;

    @ManyToMany
    @JoinTable(
            name = "service_system_version",
            joinColumns = @JoinColumn(name = "system_version_id"),
            inverseJoinColumns = @JoinColumn(name = "service_id"))
    private List<ServiceEntity> serviceEntities;
}